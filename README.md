# Annonce évènement

1. Créer un fichier markdown nommé avec la date de l'évènement dans `pages/content/post`
2. Le champ date *dans* le document doit être antérieur ou égale à la date courante (pas dans le futur) pour que le site soit mis à jour.
3. Éxécuter le script de génération des annonces avec le markdown en paramètre:

``` sh
./scipts/announce.py pages/content/post/<event_date>.md
```
4. Mail à personnel-pro@inria.fr et dev-meetup-paris@inria.fr
5. Annonce intranet [ici](https://intranet.inria.fr/myinria/suggestactuality?homepage_create_news_button=)

# Procédure pour publier sur numin

Procédure donnée par le SCM en septembre 2024 pour les developer meetups et les permanences SED.

## Requis

Avoir les droits de publication sur numin

## Etapes

Pour un article non existant

1. Aller dans l'espace "Centre Inria de Paris",
2. A l'endroit "Partager un message dans Centre Inria de Paris", cliquer sur "Articles".
3. Ecrire l'article
4. Cliquer sur le bouton "Publier"
5. Décocher "Publier sur le fil d'actualités"
6. Cliquer sur "Poursuivre"
7. On est dans la partie "Diffuser". Cocher "Diffuser dans une cible d'articles". Choisir "Centre Paris / Actualités Scientifiques"
8. Cliquer sur "Poursuivre"
9. On est dans la partie "Planifier". Choisir "Immédiatement" ou planifier la date de publication.

Pour un article existant, aller sur l'article en question.

1. Modifier l'article en cliquer sur "Modifier" dans les trois petits points en haut à droite de l'article.
2. Cliquer sur "Mettre à jour et publier".


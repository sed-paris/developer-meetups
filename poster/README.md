You need xelatex to compile `poster.tex`:
```
xelatex poster.tex
```

UbuntuMono-Regular.ttf can be downloaded from Google Fonts:
https://fonts.google.com/specimen/Ubuntu+Mono

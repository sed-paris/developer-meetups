#!/usr/bin/env python3
import datetime
import re
import sys
import locale

def talks_of_lines(lines):
    return [{
        "title": match.group("title"),
        "name": match.group("name"),
        "team": match.group("team")
    } for match in (re.search(
    "^[*] [*][*](?P<title>[^*]+)[*][*], (?P<name>[^(]+) [(](?P<team>[^)]+)[)]$",
        line) for line in lines) if match]

def session_of_file(filename):
    match = re.search(
        "/(?P<year>[0-9]{4})-(?P<month>[0-9]{2})-(?P<day>[0-9]{2})[.]md$",
        filename)
    if (match == None):
        raise Exception("No date found in filename")
    return {
        "date": datetime.date(
            int(match.group("year")), int(match.group("month")),
            int(match.group("day"))),
        "talks": talks_of_lines(open(filename))
    }

def announce_of_session(session):
    date = session["date"].strftime("%A %d %B")
    def format_talk(talk):
        result = f"* {talk['title']}, {talk['name']} ({talk['team']})"
        if len(result) > 70:
            result = f"* {talk['title']},\n  {talk['name']} ({talk['team']})"
        return result
    talks = ",\n".join(format_talk(talk) for talk in session["talks"])
    return f"""\
Subject: Developer meetup, {date}, 4pm @ Inria, Paris, building C 5th floor

Bonjour à tous,

Venez au prochain "Developer Meetup" !

**Quand ?**

{date}, 4pm @ Inria Paris, building C 5th floor

**Qui ?**

Tout le monde, et surtout vous êtes les bienvenus pour venir partager votre
expérience en matière de développement logiciel.

Le format comprend des exposés courts et des discussions ouvertes. Les sujets listés
ci-dessous sont planifiés, mais nous sommes ouverts à des présentations improvisées si vous
avez quelque chose à partager.

Les exposés seront limités à 5 minutes (ou moins), + 5 minutes pour les questions.
D'autres discussions pourront avoir lieu à la fin de l'événement, autour d'un goûter !

**Contenu**

For {date} :

{talks}.

**Pour quoi faire ?**

* Construire une communauté locale de devs ayant diverses compétences et expériences,
* Partager nos trucs et outils préférés, nos succès et nos mauvais souvenirs, etc...
* Rassembler les bonnes habitudes et pratiques de développement,
* Découvrir que la personne que vous voyez tous les jours dans l'ascenseur est en fait experte de l'outil dont vous avez besoin.

Site web : https://sed-paris.gitlabpages.inria.fr/developer-meetups/

Si vous souhaitez être informés des prochains événements SED, abonnez-vous:
https://sympa.inria.fr/sympa/subscribe/sed-paris-events


En espérant vous voir nombreux,
Le SED (Service Expérimentation et Développement) de Paris.

---

Hi everyone!

Please come to the next developer meetup !

**When?**

* {date}, 4pm @ Inria Paris, building C 5th floor *

**Who?**

Everybody, and especially *you* are welcome to come and share your
experience about code development.

The format includes lightning talks and open discussions. Talks listed
below are planned, but we are open for improvised lightning talks if you
have something to share.

Talks will be limited to be 5 minutes long (or less), plus up to 5
minutes for questions. Further discussions can take place at the end of
the event, around coffee, cakes and cookies!

**Content**:

Planned talks for {date}:
{talks}.

**What for?**

* Build a local community of coders with all kinds of skills and backgrounds,
* Share our favorite tricks and tools, success and horror stories, etc…
* Gather good coding habits and practices,
* Discover that the guy you see everyday in the elevator is actually an expert
  on that tool you need.

Web site: https://sed-paris.gitlabpages.inria.fr/developer-meetups/

To be aware of next events, feel free to subscribe:
https://sympa.inria.fr/sympa/subscribe/sed-paris-events

Looking forward to seeing you there.

--
The SED (Service Expérimentation et Développement) of Inria Paris.

"""

def iris_of_session(session):
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8") 
    french_date = session["date"].strftime("%A %d %B")
    locale.setlocale(locale.LC_TIME, "en_US.UTF-8")
    english_date = session["date"].strftime("%A %d %B")
    def format_talk(talk): 
        return f"   {talk['title']}, {talk['name']} ({talk['team']})"
    talks = "\n".join(format_talk(talk) for talk in session["talks"])
    return f"""
Le prochain developer meetup aura lieu {french_date} à 16h, au 5e étage du bâtiment C (Inria Paris).

Trois exposés courts (cinq minutes + discussions) sont prévus pour cette session :

{talks}

Café et gâteaux seront proposés comme d’habitude. Venez nombreux !

The next developer meetup will be on {english_date}, 4pm, building C 5th floor (Inria Paris).

Three short talks (5 minutes + discussions) are planned for this session :

{talks}

Coffee, cakes and cookies are provided as usual. Looking forward to seeing you there!
"""

def compro_of_session(session):
    date = session["date"].strftime("%A %d %B")
    speakers = ", ".join(talk['name'] for talk in session["talks"])
    return f"""
Bonjour !

Est-ce possible d’annoncer le prochain developer meetup sur les écrans
du centre ?

Developer Meetup
{date}, 4pm, Inria building C 5th floor
{speakers}

Merci ! Bonne fin de semaine.
-- 
Le SED de Paris.
"""

session = session_of_file(sys.argv[1])
print(f"""
Announce email:
{announce_of_session(session)}

Annonce intranet:
{iris_of_session(session)}

com-pro@inria.fr:
{compro_of_session(session)}
""")

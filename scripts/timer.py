import time
import subprocess


def play_sound_file(filename):
    subprocess.Popen(['mplayer', filename])


def wait(nb_minutes):
    nb_seconds = int(nb_minutes * 60)
    interval_seconds = 10
    for i in range(0, nb_seconds, interval_seconds):
        time.sleep(interval_seconds)
        print(f'{i+interval_seconds} seconds')


t0 = time.time()

for nb_minutes, sound_filename in [
        (4, './one-minute-left.mp3'),
        (1, './the-end.mp3'),
        (1, './cow-moo.mp3')]:
    wait(nb_minutes)
    play_sound_file(sound_filename)

duration = time.time() - t0
print(f'duration: {duration:.1f}')

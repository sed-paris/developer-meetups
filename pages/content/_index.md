## What for?

> Programming: highly satisfying activity as you think it works, after
> having spent days when it was almost working, and before you realize
> it does not actually work that well…  — Gérard Berry

Sounds familiar? Let’s talk about it at the Developer meetup!

![Developer meetups](img/developer_meetups_logo.svg)

For everybody, *once a month*, for one hour *at 4pm, building C 5th
floor*, cakes and coffee provided!

* Build a local community of coders with all kinds of skills and backgrounds,
* Share our favourite tricks and tools, success and horror stories, etc…
* Gather good coding habits and practices

Send propositions, wishes and remarks to
[sed-pro@inria.fr](mailto:sed-pro@inria.fr)

If you want to be aware of next events organized by the SED, feel free to
subscribe:
https://sympa.inria.fr/sympa/subscribe/sed-paris-events.


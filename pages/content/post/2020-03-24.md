---
title: "Session #17: 24 March 2020"
date: 2020-03-16
tags: [
  "web", "vuejs", "profiling", "python", "emacs", "email"
]
---

This session has been cancelled.

Talks :

* **Profiling Python code with `snakeviz`**, Hadrien Hendrix (SIERRA)
* **VueJS: an Adaptative Javascript Framework to Create Reactive Web Applications**, Gaël Guibon (ALMANACH)
* **Dealing with email inside emacs with `mu4e`**, Loïc Estève (SED/SIERRA)

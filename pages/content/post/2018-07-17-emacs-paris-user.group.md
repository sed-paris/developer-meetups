---
title: "Emacs Paris User Group: 17 July 2018, 7pm-10pm"
date: 2018-07-04
tags: ["emacs", "paris"]
---

Vivien Mallet sent to us the announce of the next Emacs Paris User Group meeting.

**When?** _Tuesday 17 July 2018, 7pm-10pm_.

**Where?** _[inno3.fr](https://inno3.fr), 137 Boulevard de Magenta, 75010 Paris_
([map](http://www.openstreetmap.org/#map=16/48.8818/2.3514)).

This session will be particularly well-suited for beginners.

There is a [mailing-list](https://framalistes.org/sympa/info/ateliers-emacs)
and a [website](https://www.emacs-doctor.com/emacs-paris-user-group/).
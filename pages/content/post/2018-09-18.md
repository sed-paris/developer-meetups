---
title: "Session #3: 18 September 2018"
date: 2018-09-12
tags: ["dask", "tmux", "jenkins", "pipelines"]
---

Talks:

* **Using dask for the rioc cluster**, Loïc Estève (SED).
  [Slides](https://lesteve.github.io/talks/2018-dask-dev-meetup/index.html),
  [demo Jupyter notebook](https://gist.github.com/lesteve/525110f81610357711016361b82d8069).
* **tmux**, Vivien Mallet (CLIME). tmux stands for terminal multiplexer, an
  alternative to the command-line tool called “screen”, enabling users to
  launch long-standing processes without fearing about closing the window from
  where these processes have been launched. Here is a good starting point for a
  [.tmux.conf](/2018-09-18/.tmux.conf) configuration file.
* **Continuous Integration: Jenkins Pipelines**, Thierry Martinez (SED).
  Pipelines are the modern way to setup continuous integration jobs, in a
  `Jenkinsfile` that is stored directly in the versioned repository of your
  project. [Slides](/2018-09-18/jenkins_pipelines.pdf),
  [.tex](/2018-09-18/jenkins_pipelines.tex)

---
title: "Session #15: 17 December 2019"
date: 2019-12-06
tags: [
  "dsl", "wsl", "windows", "rt", "request-tracker"
]
---

Talks:


* **When and how to make a DSL?**, Denis Merigoux (PROSECCO), [slides](/2019-12-17/dsl-merigoux.pdf)
* **Windows Subsystem for Linux**, Yasuyuki Tanaka (EVA), TBC
* **Using Inria's RT request tracker from the command-line**, Sébastien Hinderer (SED), [slides](/page/rt/)

**Bonus:** Yasuyuki Tanaka would have liked to give a talk about **Windows Subsystem for Linux** before leaving Inria. Unfortunately, he cannot come for this meetup due to personal reasons, but he was kind enough to offer us his slides: [pdf](/2019-12-17/WSL_on_Inria_Windows_PC-yatch.pdf), [pptx](/2019-12-17/WSL_on_Inria_Windows_PC-yatch.pptx).

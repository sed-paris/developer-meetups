---
title: "Session #4: 20 November 2018"
date: 2018-11-14
tags: ["shell", "jupyterlab", "git", "c++"]
---

Talks:

* **Tweaking the shell**, Loïc Estève (SED). [slides](http://lesteve.github.io/talks/2018-developer-meetup-intro-and-shell-tweaks)
* **Jupyterlab**, Sergey Zagoruyko (WILLOW). [notebook](http://nbviewer.jupyter.org/gist/szagoruyko/f09bc75cdbbfde1e923e1f1f2ea8c1ab)
* **Git workflow**, Sébastien Hinderer (SED). [slides](http://seb.thefreecat.org/slides/2018-11-20-dev-meetup-git-workflow/00.html)
* **Making New Friends**, Thierry Martinez (SED). [pdf](/2018-11-20/making_new_friends/making_new_friends.pdf) [tex](/2018-11-20/making_new_friends/making_new_friends.tex) [Dan Saks' talk at CppCon'2018.](https://www.youtube.com/watch?v=POa_V15je8Y)

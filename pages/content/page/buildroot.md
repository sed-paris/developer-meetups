# Buildroot, embedded Linux made simple

The problem:

- Carry only one device rather than two
  (a laptop and a braille display)
- Have Linux with me all the time
- Be able to upgrade the distribution easily and version-control
  the list of installed software and their configuration
- Fast boot

Solution: buildroot, a Linux distribution for embedded systems

How it works:

- Clone buildroot's repository
- Configure the distribution (Linux kernel-like configuraiton interface)
- Cross-compile everything you need (bootloader, kernel, packages)
- Burn your image to a memory card and boot your device

Your own customizations can be kept in a separate tree that will be
copied to the final file system
(layered file system)

Consequences:

- Installing packages on a running system is discouraged, rather add
  them to your configuration, so that the configuration always
  reflects what's embedded in your device

Supported hardware devices: let's see make menuconfig

Misc:

- Great documentation
- Well thought and designed,inspiring
- Uses interesting tools like ccache
- Packaging tools is well documented

---
title: Pijul
subtitle: Pierre-Étienne Meunier (TAPDANCE)
---
Presented at the developer meetup, on 19 June 2018.

Pijul is a versionning tool, based on a solid theory of asynchron
collaborative work. Pjul is based on changes (patches) rather than
states (snapshots, the way CVS, SVN, Git or Mercurial work). The state
of a branch in Pijul is entirely defined by the set of patches it
contains, and the fore-mentionned set can be edited the way the user
wants, independently of the order in which the patches were applied to
the branch.Two people with the same set of patches are thus garantied
to have exactly the same repository, no matter the order in which the
patches were produced and applied.

One of the consequences of this definition is that, in Pijul, for
every pair of patches (A, B), we either have A and B commutating, or A
explicitely dependant of B, or the reverse, B explicitely dependant of
A.

Furthermore, patches are associative: the two following operations
always do the same (which is not the case in Git):

* (Pull A, then Pull B) then Pull C
* Pull A, then (Pull B and C)

This versionning system is thus easier to use as well as more suited
to big scale projects, with numerous collaborators.
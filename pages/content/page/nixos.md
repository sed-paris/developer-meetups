---
title: NixOS
subtitle: Pierre-Étienne Meunier (TAPDANCE)
---
Presented at the developer meetup, on 15 May 2018.

Nix is a purely fonctionnal package manager, which means it’s
stateless. NixOS is a Linux distribution based on it. In Nix, packages
never interact, nor have access to the same configuration files. The
notion of package is generalized to allow different software to still
produce a common configuration file. Nix is also a small programming
language created to describ the states of a machine, which includes
configuration and installed packages.

It follows the compilations reproductibility, which allows the
detection of a change in the version of a compiler, library, …

NixOS is a Nix version designed for cloud computing, which allows to
manage machines distantly, by just installing the necessary packages,
without needing to create a full image.
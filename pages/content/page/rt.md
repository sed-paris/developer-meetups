# rt: a command-line interface to the request tracker

## Introduction

What is described here will work only on Inria's network or on the VPN.

## Installation

### Debian/Ubuntu

```
apt-get install rt4-clients
```

## Configuration

Create a file named `~/.rtrc` with the following content (see
`rt help config` for details on where the tool looks for its configuration
file and which configuration directives it supports):

```
server https://support.inria.fr/
user your_ldap_login
passwd your_ldap_password
queue sed-ci.helpdesk-PRC
```

Note that `queue` is only the defalut queue and can be overriden on the
command-line.

## Usage

The tool can be run with a command on its command-line or with no
command at all, in which case it will show a prompt where its commands
can be given -- exit with Ctrl-d.


For instance, both `rt list` and
```
rt
rt> list
```

are equivalent and will both list the tickets in the default queue.
We will thus give the examples below by givin just the rt commands. It
should be understood that they can be used both on rt's command-line
and in the rt shell.

To see the id, status, requestor and subject fields for the new
tickets in the default queue:

```
ls -f id,status,creator,subject status=New
```

## Further reading

Use the `help` command to know more about rt.

See `https://rt-wiki.bestpractical.com/wiki/Schema` for a list of
tables and fields that you can use in your queries.

The schemas can also be found in the RT sources:

```
git clone https://github.com/bestpractical/rt
cd rt
git checkout stable # should be already checked out
```

and then see e.g. `etc/schema.mysql`.

# WhichPR: find out to which pull request (PR) a commit belongs

## General context

- You work on a project that uses GitHub
- People contribute through pull requests (PRs)

Specific context:

- You have found a commit that introduces a problem (through git blame, say)
- You want to know which PR this commit belongs to

Sometimes it's written in git log:
```
commit 740baf0317e063cbd9ebb545999ad198c396a8da (origin/pr/8906)
```

But sometimes not:
```
commit 4cde3f72e29d6ec3a86da1dbc2eec279d15da6f8
```

Solution: https://github.com/pocke/whichpr

Example of use:
```
# Opens the corresponding PR in your browser
~/src/ocaml$ whichpr show 4cde3f72e29d6ec3a86da1dbc2eec279d15da6f8
8837
```

Or,
```
~/src/ocaml$ whichpr open 4cde3f72e29d6ec3a86da1dbc2eec279d15da6f8
```

## Installation notes

- Follow the instructions in `README.md`
- Do not try to install from sources, it didn't work for me on Debian
- Rather download the binary and put the whichpr executable somewhere in
  your `PATH`, e.g. `~/bin`
- I created my authorization token directly on GitHub,
  didn't want to give my password to the tool


Here is my `~/.config/whichpr` file:

```
github.com:
- user: shindere
  oauth_token: MYVERYSECRETTOKENCREATEDONGITHUB
  protocol: https
```

I find the syntax odd but it works that way

Happy hacking!

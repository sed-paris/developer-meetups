// Copyright 2017 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This program generates Mandelbrot fractals using different
// concurrency patterns.
package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"os"
	"sync"
	"time"
)

const (
	output1    = "out_seq.png"
	output2    = "out_parll.png"
	width      = 2048
	height     = 2048
	numWorkers = 12
)

func main() {
	// uncomment these lines to generate CPU profile or traces.

	// file, err := os.Create("cpu.pprof")
	// pprof.StartCPUProfile(file)
	// defer file.Close()
	// defer pprof.StopCPUProfile()

	// file, err := os.Create("main.trace")
	// trace.Start(file)
	// defer file.Close()
	// defer trace.Stop()

	startseq := time.Now()

	f, err := os.Create(output1)
	if err != nil {
		log.Fatal(err)
	}

	img := createSeq(width, height)

	if err = png.Encode(f, img); err != nil {
		log.Fatal(err)
	}

	f.Close()

	seqTime := time.Since(startseq)

	fmt.Printf("Sequential execution time: %v\n", seqTime)

	startParll := time.Now()

	g, err := os.Create(output2)
	if err != nil {
		log.Fatal(err)
	}

	img2 := createColWorkersBuffered(width, height)

	if err = png.Encode(g, img2); err != nil {
		log.Fatal(err)
	}

	g.Close()

	timeParll := time.Since(startParll)

	fmt.Printf("Parallel execution time: %v\n", timeParll)

	fmt.Println("Fun fact: Did I tell you that Go supports UTF8 out of the box? 世界")
}

// createSeq fills one pixel at a time.
func createSeq(width, height int) image.Image {
	m := image.NewGray(image.Rect(0, 0, width, height))
	for i := 0; i < width; i++ {
		for j := 0; j < height; j++ {
			m.Set(i, j, pixel(i, j, width, height))
		}
	}
	return m
}

// createColWorkersBuffered creates numWorkers workers and uses a buffered channel to pass each column.
func createColWorkersBuffered(width, height int) image.Image {
	m := image.NewGray(image.Rect(0, 0, width, height))

	c := make(chan int, width)

	var w sync.WaitGroup
	for n := 0; n < numWorkers; n++ {
		w.Add(1)
		go func() {
			for i := range c {
				for j := 0; j < height; j++ {
					m.Set(i, j, pixel(i, j, width, height))
				}
			}
			w.Done()
		}()
	}

	for i := 0; i < width; i++ {
		c <- i
	}

	close(c)
	w.Wait()
	return m
}

// pixel returns the color of a Mandelbrot fractal at the given point.
func pixel(i, j, width, height int) color.Color {
	// Play with this constant to increase the complexity of the fractal.
	// In the justforfunc.com video this was set to 4.
	const complexity = 1024

	xi := norm(i, width, -1.0, 2)
	yi := norm(j, height, -1, 1)

	const maxI = 1000
	x, y := 0., 0.

	for i := 0; (x*x+y*y < complexity) && i < maxI; i++ {
		x, y = x*x-y*y+xi, 2*x*y+yi
	}

	return color.Gray{uint8(x)}
}

func norm(x, total int, min, max float64) float64 {
	return (max-min)*float64(x)/float64(total) - max
}

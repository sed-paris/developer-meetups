---
marp: true
paginate: true
---

# An introduction to Kubernetes

Sacha Bernheim - April 16th 2024

---

<div style="display: flex; height: 100%;">
  <div style="flex: 1; min-width: 50%;">
    <img src="assets/qat.png" style="width: 100%; height: 100%; object-fit: cover;">
  </div>
  <div style="flex: 1; min-width: 50%; padding-left: 20px; background: white;">
    <h1>About Me</h1>
    <ul>
      <li>Former Senior SRE at Padok</li>
      <li>M2 Intern in Quantum Information at QAT Team</li>
    </ul>
  </div>
</div>

---

# How to deploy?

<table>
    <tr>
        <th style="width: 20%;"> </th>
        <th style="width: 16%;">C++</th>
        <th style="width: 16%;">Java</th>
        <th style="width: 16%;">Golang</th>
        <th style="width: 16%;">Python</th>
        <th style="width: 16%;">Julia</th>
    </tr>
    <tr>
        <td>Web Applications</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
    </tr>
    <tr>
        <td>Data Processing Applications</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
    </tr>
    <tr>
        <td>Others</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
        <td>?</td>
    </tr>
</table>

---

# Another industry had a similar issue...

<div style="display: flex; justify-content: space-between; align-items: center;">
  <img src="assets/container.png" alt="Container" style="width: 50%;">
  <img src="assets/docker.png" alt="Docker" style="width: 50%;">
</div>


---

# How to deploy?

<table>
    <tr>
        <th style="width: 20%;"> </th>
        <th style="width: 16%;">C++</th>
        <th style="width: 16%;">Java</th>
        <th style="width: 16%;">Golang</th>
        <th style="width: 16%;">Python</th>
        <th style="width: 16%;">Julia</th>
    </tr>
    <tr>
        <td>Web Applications</td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
    </tr>
    <tr>
        <td>Data Processing Applications</td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
    </tr>
    <tr>
        <td>Others</td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
        <td><img src="assets/docker.png" style="width: 100%;"></td>
    </tr>
</table>

---

# Is it enough?

- **Handling Failures and Restarts**: How to ensure continuity?
- **Handling Cardinality**: How to manage scaling?

---

# Enter Kubernetes

<div align="center">
    <img src="assets/kubernetes.png" style="width: 65%;">
</div>

<div align="center">
Open-source solution for deploying softwares across multiple (Linux) machines. 
</div>
<div align="center">
<strong>Container orchestrator</strong> with an <strong>extensible declarative API</strong>
</div>

---

# Kubernetes architecture overview

<div align="center">
    <img src="assets/kube-architecture.png" style="width: 65%;">
</div>

---

# Kubernetes: A Declarative CRUD API

- Establish new resources in etcd through the API.
- Retrieve the current state of resources directly from etcd.
- Modify resources in etcd via the API to match the desired state.
- Erase resources from etcd when they're no longer needed.

---

# Reconciliation Loops

- Kubernetes controllers constantly monitor the state of resources.
- Any discrepancies between the declared state and the actual state are addressed.
- Automatically corrects deviations, ensuring reliability and stability.

---

# Extensible API with CRDs

- Define and manage your own resources, expanding Kubernetes capabilities.
- Use CRDs to introduce custom operational knowledge and workflows. The open-source ecosystem is **HUGE**.
- Tailor Kubernetes to meet the specific needs of your project.

---

# Pod: The Atomic Unit

<div style="display: flex;">
  <div style="flex: 50%; padding-right: 20px;">
    <pre><code>apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    app: nginx
spec:
  containers:
  - name: nginx-container
    image: nginx:1.14.2
    ports:
    - containerPort: 80</code></pre>
  </div>
  <div style="flex: 50%;">
    <ul>
      <li>Host closely related containers that need to work together.</li>
      <li>Generally ephemeral and created or destroyed to match the desired state.</li>
      <li>Share data between containers in a Pod.</li>
    </ul>
  </div>
</div>

---

# Deployments for Managing Pods

<div style="display: flex;">
  <div style="flex: 50%; padding-right: 20px;">
    <pre><code>apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
</code></pre>
  </div>
  <div style="flex: 50%;">
    <ul>
      <li>Deploy new versions of your application.</li>
      <li>Easily revert to a previous version if something goes wrong.</li>
      <li>Handle increasing load by scaling out pods.</li>
    </ul>
  </div>
</div>

---

# Service: Consistent Access Point

<div style="display: flex;">
  <div style="flex: 50%; padding-right: 20px;">
    <pre><code>apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer</code></pre>
  </div>
  <div style="flex: 50%;">
    <ul>
      <li>Pods within a cluster can access services through their stable endpoints.</li>
      <li>Services can load-balance traffic to multiple Pod instances.</li>
      <li>Expose Pods to the internet or keep them only within the cluster.</li>
    </ul>
  </div>
</div>

---

# ConfigMap: Configuration Data


<div style="display: flex;">
  <div style="flex: 50%; padding-right: 20px;">
    <pre><code>apiVersion: v1
kind: ConfigMap
metadata:
  name: app-config
data:
  log_level: "INFO"
  enable_debug: "true"</code></pre>
  </div>
  <div style="flex: 50%;">
    <ul>
      <li>Store configuration files, command-line arguments, environment variables.</li>
      <li>Update configurations without changing the container image.</li>
      <li>Easily integrated with Pods to provide configuration data.</li>
    </ul>
  </div>
</div>

---

# A lot of others resources

HorizontalPodAutoscaler, Namespace, RBAC resources, Secret, Ingress...

---

# Useful references

- [Phippy](https://www.cncf.io/phippy/)
- Kubernetes in Action, Marko Lukša. New edition by the end of the year...
- [The Kubernetes documentation](https://kubernetes.io/)

---

# It's Q&A time!

---
marp: true
title: "Introduction to nbIntroduction to nb: a command line tool for note-taking"
author: Kim Tâm HUYNH
theme: default
---

# Introduction to nb

A command line tool for note-taking

---

<!-- paginate: true -->

## What is nb ?

[nb](https://xwmx.github.io/nb/) is a **command line** and **local** knowledge base application :
- web note‑taking,
- bookmarking,
- archiving,
- and more, in a single portable script.

> Implements [Zettelkasten](https://en.wikipedia.org/wiki/Zettelkasten), also named "card file" : method using index cards, crosslinks, and metadata for managing a personal knowledge base.

---

## Some Key Concepts

- Organization by notebooks
    - a notebook is a `git` repository containing a `.index` file
- 3 types of items (i.e., text files): notes, bookmarks, todos
    - `tasks` are sub-items of todos files or notes (using `- [ ]` mark)
- These items can be organized in folders (containing `.pindex` file)
- Operations: add/create, list, edit/update, view, delete


### Configuration

- `nb` directory location
- text editor: atom, code, emacs, hx, vim, ...

---

### Image example from nb documentation

<!--
This image is not essential for understanding, it allows to provide audience with a visual overview of what nb looks like.
Ref: https://xwmx.github.io/misc/nb/images/nb-theme-nb-home.png
-->

![auto height:15cm](image.png)

---

## Some basic commands : Adding

- Adding a note

```shell
nb add
nb add example.md
nb add "This is a note."
```
- Adding a todo

```shell
nb todo add "Example todo one."
```

- Adding a bookmark (with the page's content)
```shell
nb https://example.com
```

---

## Some basic commands : Viewing, Editing & Deleting

- Viewing/editing/deleting a note

```shell
nb show/edit/delete example.md
nb show/edit/delete 3
nb show/edit/delete example:12
```
\+ moving, renaming, etc.

- Changing notebook context

```shell
nb use drafts  # Change to 'drafts' notebook
```

---

## Some basic commands : Listing & Searching

- Listing notebooks and note files

```shell
nb ls
```

- Searching (default: `git-grep`)
```shell
# search all unarchived notebooks for "example query" and list matching items
nb search "example query" --all --list

# search for "example" AND "demo" with multiple arguments
nb search "example" "demo"

# search with a regular expression
nb search "\d\d\d-\d\d\d\d"
```
---

## Advanced features

- terminal and GUI web browsing (default: w3m) : `nb browse`,
- filtering, pinning (`nb pin`), #tagging (using `#hashtags`), and search,
- [[wiki-style linking]],
- archiving notebooks
- Git-backed versioning and syncing,
- Pandoc-backed conversion (if installed),
- global and local notebooks, import/export
- encryption: openssl, gpg, config,
- customizable color themes,
- extensibility through plugins, ...


---

## Why nb ? (1/3)

My habits:
- I often switch from subject to another in the same day, some notes are in draft state
    - I used to version with `git` but manually => sometimes, I'm lost in my notes
- I'm often in the terminal, I don't like to have too many apps open

---


## Why nb ? (2/3)

My expectations:
- only **one** tool for **all my notes** : ideas, works in progress, reports, ...
- features:
    - local/offline with versioning,
    - text format (markdown),
    - browsable outside the tool,
    - easy to retrieve

---

## Why nb ? (3/3)

I tried:
- **org-mode** but emacs learning curve is too high for me
- some **vim plugins** but it requires to have new habits on shortcuts,
- bash aliases (with vi) for adding notes fastly (like a diary) => limited, because I have to add scripts each time I have a new need.
- etc.

Finally, I have discovered `nb` few months ago:
- it is very simple to use, + it's based on git for versioning (**it's great !**)
- we can start very quickly and learn advanced features later.

---
<!-- paginate: false -->

# Thanks

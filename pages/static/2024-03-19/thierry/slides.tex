\documentclass{beamer}
\usepackage{fontspec,minted}
\begin{document}
\begin{frame}
  \title{Structural pattern matching in Python:\\a reconstruction}
  \author{Thierry Martinez, QAT/SED}
  \date{Tuesday 19 March, Inria Paris developer meetup}
  \maketitle
\end{frame}
\begin{frame}[fragile]
  \frametitle{Structural pattern matching in Python}

  Pattern matching originates from functional languages and has been
  adopted across various programming languages.

  \vfill

  It is a convenient construct for case analysis and data destructuring.

  \vfill

  \begin{minted}{python}
match x:
    case []:
        print("Empty list")
    case [Point(x, y)]:
        print(f"Single point: {x}, {y}")
    case [Point(x1, y1), Point(x2, y2)] if y1 = y2:
        print(f"Horizontal line: {x1} - {x2}, {y1}")
    case int(i):
        print(f"Integer: {i}")
    case _:
        print("Something else")
  \end{minted}
\end{frame}
\begin{frame}
  \frametitle{Structural pattern matching in Python: history}
  \begin{itemize}
  \item \textbf{Proposal:} 23 June 2020\\
    Brandt Bucher \emph{et alii},\\ \href{https://peps.python.org/pep-0622/}{PEP 622 – Structural Pattern Matching}
    \vfill
  \item \textbf{First Specification:} 12 September 2020\\
    Brandt Bucher, Guido van Rossum,\\ \href{https://peps.python.org/pep-0634/}{PEP 634 – Structural Pattern Matching: Specification}\\
    ``\emph{This PEP is a historical document.}''
    \vfill
  \item \textbf{Released:} 4 October 2021, Python 3.10
    \vfill
  \item \textbf{Current documentation (and specification):}\\
    \href{https://docs.python.org/3/reference/compound_stmts.html\#match}{Python documentation}, \emph{¶ 8.6. The match statement}
  \end{itemize}
\end{frame}
\begin{frame}[fragile]
  \frametitle{Motivation for a reconstruction}
Pull Request on GitHub project \texttt{graphix}:\\
\emph{Refactor measure operator in a new pauli module} \#122\\
\url{https://github.com/TeamGraphix/graphix/pull/122}
\vfill
  \begin{minted}{python}
match a, b:
    case Axis.X, Axis.Y:
        return Plane.XY
    case Axis.Y, Axis.Z:
        return Plane.YZ
    case Axis.X, Axis.Z:
        return Plane.XZ
  \end{minted}
\vfill
In the pull-request discussion, \texttt{@shinich1}, the principal developer, said:
\emph{[...] it's good to keep supporting 3.9.}
\vfill
Is there a refactor tool to \textbf{translate \mintinline{python}{match}-blocks to code compatible with Python prior to 3.10}?
\end{frame}
\begin{frame}
  \frametitle{A more fundamental motivation: curiosity!}

  \begin{itemize}
  \item I haven't found such a tool: the goal now is to build a custom
    refactor tool.
    \vfill
  \item An opportunity to learn about the \textbf{general frameworks for
    building a refactor tool} for Python.
    \vfill
  \item An opportunity to \textbf{dive into the Python pattern-matching
    specification}.
\end{itemize}
\end{frame}
\begin{frame}
  \frametitle{General Frameworks for Refactoring Python Code}

  \begin{itemize}
  \item \href{https://pybowler.io/}{\textbf{Bowler}} \emph{(outdated)}:
    \begin{itemize}
    \item Built on \href{https://github.com/amyreese/fissix}{fissix}, a backport of \texttt{lib2to3} removed from the standard Python library in Python 3.10.
    \item Does not support the new PEG-based parser introduced in 3.10.
    \end{itemize}

    \vfill

  \item \href{https://github.com/Instagram/LibCST}{\textbf{LibCST}}, open-source project from \emph{Instagram}:
    \begin{itemize}
    \item A Concrete Syntax Tree (CST) parser and serializer library for Python.
    \item Loss-less parser: keeps all formatting details (comments, whitespaces, parentheses, etc.).
    \item Aims to be as convenient as an Abstract Syntax Tree (AST).
    \item Functional flavour: mypy-compliant, structures are immutable\\
      (functional update with \mintinline{python}{node.with_changes(key=value)}...
    \end{itemize}
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{\texttt{match\_transformer} tool}

  \url{https://github.com/thierry-martinez/match_transformer}

  \vfill

  \begin{itemize}
  \item A refactor tool based on LibCST (keeps all formatting details).
  \vfill
  \item Translate all \mintinline{python}{match}-blocks into legacy code.
  \vfill
  \item Passes (almost) all the \texttt{Python pattern-matching test-suite}:
    \vfill
    \begin{itemize}
    \item Dynamic parsing of \mintinline{python}{match}-blocks (via \mintinline{python}{eval}) is not supported.
      \vfill
    \item Traces are not preserved (tests that use \mintinline{python}{_trace()} method to track line-numbers in traces are broken).
    \end{itemize}
    \vfill
  \item No code duplication, \textbf{preserves flow-control} (\mintinline{python}{break}, \mintinline{python}{continue}, \mintinline{python}{return}) and \textbf{context} (\mintinline{python}{globals()} and \mintinline{python}{locals()}).
    \vfill
  \item Not much room for performance optimisation.
  \end{itemize}
\end{frame}
\begin{frame}[fragile]
  \frametitle{Generated code is mostly readable and can be used in commits}

  \begin{minted}{python}
        # match a, b:
        #     case Axis.X, Axis.Y:
        #         return Plane.XY
        #     case Axis.Y, Axis.Z:
        #         return Plane.YZ
        #     case Axis.X, Axis.Z:
        #         return Plane.XZ
        if a == Axis.X and b == Axis.Y:
            return Plane.XY
        elif a == Axis.Y and b == Axis.Z:
            return Plane.YZ
        elif a == Axis.X and b == Axis.Z:
            return Plane.XZ
  \end{minted}
\end{frame}
\begin{frame}[fragile]
  \frametitle{LibCST and pretty-printing}

  LibCST is quite convenient for building syntax trees and pretty-printing.

  \vfill

  However, we should keep in mind that it is a \textbf{Concrete Syntax Tree}, and the validators are not complete (though they are still present).

  \vfill

\footnotesize


  \begin{minted}{python}
>>> m = cst.Module([])
>>> m.code_for_node(
...     cst.BinaryOperation(
...         cst.Integer("1"), cst.Multiply(),
...         cst.BinaryOperation(
...             cst.Integer("2"), cst.Add(), cst.Integer("3"))))
'1 * 2 + 3'
>>> m.code_for_node(
...     cst.BinaryOperation(
...         cst.Integer("1"), cst.Multiply(),
...         cst.BinaryOperation(
...             cst.Integer("2"), cst.Add(), cst.Integer("3"),
...             lpar=[cst.LeftParen()],
...             rpar=[cst.RightParen()])))
'1 * (2 + 3)'
  \end{minted}
\end{frame}
\begin{frame}[fragile]
  \frametitle{Pattern-matching allows dictionary key functional removal}

  \begin{minted}{python}
match {"a": 1, "b": 2}:
    case {"a": _, **d}:
        assert d == {"b": 2}
  \end{minted}

\vfill

Generated code:
  \begin{minted}{python}
d = [key: value for key, value in subject.items()
    if key not in {"a"}]
  \end{minted}
  
\end{frame}
\begin{frame}[fragile]
  \frametitle{Failed bindings are specified irrelevant... but tested}

The documentation says:\\
\emph{\textbf{Note:} During failed pattern matches, some subpatterns may succeed. Do not rely on bindings being made for a failed match. Conversely, do not rely on variables remaining unchanged after a failed match.}

\vfill

\small
  \begin{columns}
    \begin{column}{.5\textwidth}
  \begin{minted}{python}
def test_patma_042(self):
    x = 2
    y = None
    match x:
        case (0 as z) |
            (1 as z) |
            (2 as z) if
                z == x % 2:
            y = 0
    self.assertEqual(x, 2)
    self.assertIs(y, None)
    self.assertEqual(z, 2)
  \end{minted}
\end{column}
    \begin{column}{.5\textwidth}
  \begin{minted}{python}
test = subject == 0 or
    subject == 1 or
    subject == 2
if test: z = subject 
if test and z == x % 2:
    del subject
    del test
    y = 0
else:
    del test
    del subject
  \end{minted}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[fragile]
  \frametitle{Proper handling of side-effects}
\small
  \begin{columns}
    \begin{column}{.5\textwidth}
  \begin{minted}{python}
def test_patma_081(self):
    x = 0
    match x:
        case 0 if not (x := 1):
            y = 0
        case (0 as z):
            y = 1
    self.assertEqual(x, 1)
    self.assertEqual(y, 1)
    self.assertEqual(z, 0)
  \end{minted}
\end{column}
    \begin{column}{.5\textwidth}
  \begin{minted}{python}
subject = (x)
if subject == 0 and not (x := 1):
    del subject
    y = 0
elif subject == 0:
    z = subject
    del subject
    y = 1
else:        del subject
  \end{minted}
\end{column}
\end{columns}
\end{frame}
\begin{frame}[fragile]
  \frametitle{Sequences and mappings: do not believe the specification!}

The specification says:

{\em\footnotesize
In pattern matching, a sequence is defined as one of the following:

\begin{itemize}
\item  a class that inherits from \mintinline{python}{collections.abc.Sequence}

\item a Python class that has been registered as collections.abc.Sequence

\item a builtin class that has its (CPython) \mintinline{c}{Py_TPFLAGS_SEQUENCE} bit set

\item a class that inherits from any of the above
\end{itemize}}

In practice, \mintinline{python}{match} only tests for
\mintinline{c}{Py_TPFLAGS_SEQUENCE} and \mintinline{c}{Py_TPFLAGS_MAPPING}, that
are mutually exclusive (but not accessible in pure Python).

\vfill
{
\footnotesize
\begin{minted}{python}
class M1(collections.UserDict, collections.abc.Sequence):
    pass
match x:
    case [*_]: # do not match
        return "seq"
    case {}:
        return "map" # it is a map!
\end{minted}
}
\vfill

Use \mintinline{python}{M1.__mro__} for depth-first search of collection class in ancestors.

\end{frame}
\begin{frame}
  \frametitle{Conclusion}

  \begin{itemize}
  \item \texttt{LibCST} is a convenient library for automating complex Python code refactoring.
    \vfill
  \item We can use \mintinline{python}{match}-blocks in projects using Python versions earlier than 3.10 and employ \texttt{match\_transformer} for their translation.
    \vfill
  \item Python has some very peculiar corner cases, even in the newly-designed parts of the language!
    \vfill
  \item It would be interesting to have a tool in the opposite direction, that transforms \mintinline{python}{if-elif-else} chains into \mintinline{python}{match}-blocks.
  \end{itemize}
\end{frame}
\end{document}

%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
